import 'package:apps_bloc/model/photo.dart';
import 'package:http/http.dart' as http;

class PhotoRepository {
  Future<List<PhotosModel>?> getPhotos() async {
    final uri =
        Uri.parse("https://jsonplaceholder.typicode.com/albums/1/photos");
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      return photosModelFromJson(response.body);
    } else {
      throw Exception("cant get data");
    }
  }

  Future<List<PhotosModel>> getDetail({int? photosID}) async {
    final uri =
        Uri.parse("https://jsonplaceholder.typicode.com/photos/?id=$photosID");
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      return photosModelFromJson(response.body);
    } else {
      throw Exception("cant get data");
    }
  }
}
