import 'dart:convert';

import 'package:http/http.dart' as http;

class AuthRepository {
  login(
      String email, String password, String device_id, String companyID) async {
    final url = Uri.parse("http://47.74.214.215:82/mass-approval/api/login");
    var response = await http.post(url, headers: {}, body: {
      "email": email,
      "password": password,
      "device_id": device_id,
      "companyID": companyID
    });

    final data = json.decode(response.body);

    if (data['info'] == "success") {
      // return data;
      return data;
    } else {
      return "auth problem";
    }
  }
}
