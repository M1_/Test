import 'package:intl/intl.dart';
import 'dart:convert';

void main() {
  //separator
  var formatter = NumberFormat('#,000');
  print(formatter.format(77777));

//replacement string
  var word1 = "Drajat jatmiko";
  var rep = "Drajat jatmiko";
  var word2 = "Morning Glory Enterprise Developer";
  final newString = word1.replaceAll(rep, word2);
  print(newString);

  print("------------");

  var rep2 = "o";
  var newWord = "A";
  var newString2 = newString.replaceAll(rep2, newWord);
  print(newString2);

//Array
  var myList;
  myList = [];
  print(myList);
  myList.addAll({'Surabaya', 'Jakarta', 'Semarang', 'Makasar'});
  print(myList);
  print("-------------------------------");
  myList[2] = "Samarinda";
  print(myList);
  print("--------------");
  myList.add("Aceh");
  print(myList);
}

void test() {
  Vehicle _nama = new Vehicle.namedConst('Drajat jatmiko');
  Vehicle _kapasitas_mesin = new Vehicle.namedKM('90 KM');
  Vehicle _roda = new Vehicle.namedConstttt('4444');
}

class Vehicle {
  Vehicle.namedConst(String nama) {
    print("Nama : ${nama}");
  }
  Vehicle.namedKM(String _kapasitas_mesin) {
    print("Kapasitas Mesin : ${_kapasitas_mesin}");
  }
  Vehicle.namedConstttt(String roda) {
    print("Roda : ${roda}");
  }
}
