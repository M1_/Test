part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class StartEvent extends AuthEvent {}

class LoginButtonPressedEvent extends AuthEvent {
  final String email;
  final String password;
  final String device_id;
  final String companyID;

  LoginButtonPressedEvent(
      {required this.email,
      required this.password,
      required this.device_id,
      required this.companyID});
}
