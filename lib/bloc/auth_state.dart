part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class LoginInitState extends AuthState {}

class LoginLoadingState extends AuthState {}

class LoginSucces extends AuthState {}

class LoginError extends AuthState {
  final String? message;

  LoginError({this.message});
}
