import 'package:apps_bloc/repository/auth.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthRepository? repo;
  AuthBloc(AuthState initialState, this.repo) : super(LoginInitState());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is StartEvent) {
      yield LoginInitState();
    } else if (event is LoginButtonPressedEvent) {
      yield LoginLoadingState();
      var data = await repo!
          .login(event.email, event.password, event.device_id, event.companyID);
      if (data["info"] == "success") {
        yield LoginSucces();
      } else {
        yield LoginError(message: "Login error");
      }
    }
  }
}
