part of 'post_bloc.dart';

abstract class InfiniteLoadEvent extends Equatable {
  const InfiniteLoadEvent();

  @override
  List<Object> get props => [];
}

class GetInfiniteLoad extends InfiniteLoadEvent {}

class GetMoreInfiniteLoad extends InfiniteLoadEvent {
  GetMoreInfiniteLoad();

  @override
  List<Object> get props => [];
}

class LoadPhotosDetail extends InfiniteLoadEvent {
  const LoadPhotosDetail({required this.photosID});

  final int photosID;

  @override
  List<Object> get props => [photosID];

  @override
  String toString() {
    return super.toString();
  }
}
