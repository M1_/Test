import 'dart:convert';
import 'dart:math';

import 'package:apps_bloc/model/photo.dart';
import 'package:apps_bloc/repository/photos_repository.dart';
import 'package:apps_bloc/utils/api.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

part 'post_event.dart';
part 'post_state.dart';

class InfiniteLoadBloc extends Bloc<InfiniteLoadEvent, InfiniteLoadState> {
  InfiniteLoadBloc() : super(InfiniteLoadLoading());
  PhotoRepository _repository = PhotoRepository();
  List<PhotosModel> _data = [];
  int? _currentLenght;

  @override
  InfiniteLoadState get initialState => InfiniteLoadLoading();

  @override
  Stream<Transition<InfiniteLoadEvent, InfiniteLoadState>> transformEvents(
      Stream<InfiniteLoadEvent> events, transitionFn) {
    return super.transformEvents(
        events.debounceTime(const Duration(milliseconds: 500)), transitionFn);
  }

  @override
  Stream<InfiniteLoadState> mapEventToState(
    InfiniteLoadEvent event,
  ) async* {
    if (event is GetInfiniteLoad) {
      yield* _mapEventToStateInfiniteLoad();
    } else if (event is GetMoreInfiniteLoad) {
      yield* _mapEventToStateInfiniteLoad();
    } else if (event is PhotosDetailLoading) {
      yield* _mapEventToStateDetailLoad(1);
    }
  }

  Stream<InfiniteLoadState> _mapEventToStateInfiniteLoad() async* {
    try {
      final reqData = await _repository.getPhotos();

      if (reqData!.isNotEmpty) {
        _data.addAll(reqData);
        print(_data);
      } else {}
      yield InfiniteLoadLoaded(data: _data);
    } catch (e) {
      yield InfiniteLoadError();
    }
  }

  Stream<InfiniteLoadState> _mapEventToStateDetailLoad(int photosID) async* {
    try {
      final reqData = await _repository.getDetail(photosID: photosID);

      if (reqData.isNotEmpty) {
        _data.addAll(reqData);
        print(_data);
      } else {}
      yield PhotosDetailLoaded(photosID: _data);
    } catch (e) {
      yield PhotosLoadedFieled();
    }
  }
}
