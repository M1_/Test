part of 'post_bloc.dart';

abstract class InfiniteLoadState extends Equatable {
  const InfiniteLoadState();
  @override
  List<Object> get props => [];
}

class InfiniteLoadLoading extends InfiniteLoadState {}

class InfiniteLoadMoreLoading extends InfiniteLoadState {}

class InfiniteLoadLoaded extends InfiniteLoadState {
  final List<PhotosModel> data;

  InfiniteLoadLoaded({
    required this.data,
  });
  @override
  List<Object> get props => [data];
}

class InfiniteLoadError extends InfiniteLoadState {}

class PhotosDetailLoading extends InfiniteLoadState {}

class PhotosDetailLoaded extends InfiniteLoadState {
  final List<PhotosModel> photosID;
  const PhotosDetailLoaded({required this.photosID});

  @override
  List<Object> get props => [photosID];
}

class PhotosLoadedFieled extends InfiniteLoadState {}
