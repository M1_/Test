import 'package:apps_bloc/model/photo.dart';
import 'package:apps_bloc/moduls/bloc/post_bloc.dart';
import 'package:apps_bloc/moduls/home/detail/detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  late InfiniteLoadBloc _bloc;
  List<PhotosModel> _data = [];

  void _loadMoreData() {
    _bloc.add(GetMoreInfiniteLoad());
  }

  @override
  void initState() {
    _bloc = BlocProvider.of<InfiniteLoadBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<InfiniteLoadBloc, InfiniteLoadState>(
      builder: (context, state) {
        if (state is InfiniteLoadLoaded || state is InfiniteLoadMoreLoading) {
          if (state is InfiniteLoadLoaded) {
            _data = state.data;
          }
          return _buildListPhotos(state);
        } else if (state is InfiniteLoadLoading) {
          return const Center(child: CircularProgressIndicator());
        } else {
          return Text('Error');
        }
      },
    );
  }

  Widget _buildListPhotos(InfiniteLoadState state) {
    return LazyLoadScrollView(
      child: ListView(
        children: [
          ListView.builder(
              shrinkWrap: true,
              itemCount: _data.length,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (_, i) {
                return InkWell(
                  child: PhotoItem(data: _data[i]),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                DetailScreen(data: _data[i])));
                  },
                );
              }),
          // Loading indicator more load data
          (state is InfiniteLoadMoreLoading)
              ? const Center(child: CircularProgressIndicator())
              : const SizedBox(),
        ],
      ),
      onEndOfPage: _loadMoreData,
    );
  }
}

class PhotoItem extends StatelessWidget {
  final PhotosModel data;

  const PhotoItem({
    Key? key,
    required this.data,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
          child: ListTile(
            leading: Image.network('${data.thumbnailUrl}'),
            title: Text(data.title),
          ),
        ),
      ),
    );
  }
}
// return Card(
//       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
//       child: Padding(
//         padding: const EdgeInsets.all(12.0),
//         child:
//             Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
//           Image.network(
//             '${data.thumbnailUrl}',
//             fit: BoxFit.cover,
//             height: 150,
//           ),
//           SizedBox(height: 8.0),
//           Text(data.title),
//         ]),
//       ),
//     );