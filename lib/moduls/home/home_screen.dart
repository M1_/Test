import 'package:apps_bloc/moduls/bloc/post_bloc.dart';
import 'package:apps_bloc/moduls/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.menu),
        title: const Text('Apps'),
        centerTitle: true,
        actions: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Icon(Icons.search),
          ),
          Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: new IconButton(
                  icon: const Icon(Icons.logout_outlined),
                  highlightColor: Colors.pink,
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, '/');
                  }))
        ],
      ),
      body: BlocProvider(
        create: (context) => InfiniteLoadBloc()..add(GetInfiniteLoad()),
        child: HomePage(),
      ),
    );
  }
}
