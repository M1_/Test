import 'package:apps_bloc/model/photo.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatelessWidget {
  final PhotosModel data;
  const DetailScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text("Detail")),
        centerTitle: true,
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 12.0),
        child: Column(
          children: [
            Image.network(data.thumbnailUrl),
            Text(data.title),
            Text(data.url)
          ],
        ),
      ),
    );
  }
}
